package com.connector

import java.util.{List => JavaList, Map => JavaMap}

import scala.collection.JavaConverters._
import org.apache.kafka.common.config.ConfigDef
import org.apache.kafka.common.config.ConfigDef._
import org.apache.kafka.connect.connector.Task
import org.apache.kafka.connect.errors.ConnectException
import org.apache.kafka.connect.source.SourceConnector
import org.slf4j.LoggerFactory

import scala.util.{Failure, Success, Try}

class HttpSourceConnector extends SourceConnector {
  private val connectorLogger = LoggerFactory.getLogger(getClass)

  private var connectorConfig: HttpSourceConnectorConfig = _

  override def start(map: JavaMap[String, String]): Unit = {
    Try(new HttpSourceConnectorConfig(map.asScala.toMap)) match {
      case Success(cfg) => connectorConfig = cfg
      case Failure(err) => connectorLogger.error(s"Could not start Kafka Source Connector ${this.getClass.getName}" +
        s" due to error in configuration.", new ConnectException(err))
    }
  }

  override def taskClass(): Class[_ <: Task] = classOf[HttpSourceTask]

  override def taskConfigs(i: Int): JavaList[JavaMap[String, String]] = List(connectorConfig.connectorProperties.asJava).asJava

  override def stop(): Unit = connectorLogger.info(s"Stopping Kafka Source Connector ${getClass.getName}.")

  override def config(): ConfigDef = configDef

  private val configDef = new ConfigDef()
    .define(HttpSourceConnectorConstants.HTTP_URL_CONFIG, Type.STRING, Importance.HIGH, "Web API Access URL")
    .define(HttpSourceConnectorConstants.API_KEY_CONFIG, Type.STRING, Importance.HIGH, "Web API Access Key")
    .define(HttpSourceConnectorConstants.API_PARAMS_CONFIG, Type.STRING, Importance.HIGH, "Web API additional config parameters")
    .define(HttpSourceConnectorConstants.SERVICE_CONFIG, Type.STRING, Importance.HIGH, "Kafka Service name")
    .define(HttpSourceConnectorConstants.TOPIC_CONFIG, Type.STRING, Importance.HIGH, "Kafka Topic name")
    .define(HttpSourceConnectorConstants.POLL_INTERVAL_MS_CONFIG, Type.STRING, Importance.HIGH, "Polling interval in milliseconds")
    .define(HttpSourceConnectorConstants.TASKS_MAX_CONFIG, Type.INT, Importance.HIGH, "Kafka Connector Max Tasks")
    .define(HttpSourceConnectorConstants.CONNECTOR_CLASS, Type.STRING, Importance.HIGH, "Kafka Connector Class Name (full class path)")

  override def version(): String = HttpSourceVersion.getVersion
}

