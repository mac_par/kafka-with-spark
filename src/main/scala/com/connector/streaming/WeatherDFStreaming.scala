package com.connector.streaming

import com.connector.schema.WeatherParser
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Minutes, StreamingContext}

object WeatherDFStreaming {
  def main(args: Array[String]): Unit = {
    val cfg = new SparkConf().setMaster("local[*]").setAppName("WeatherWorker")
    val sc = new StreamingContext(cfg, Minutes(1))
    sc.sparkContext.setLogLevel("ERROR")

    val kafkaParams = Map[String, Object](
      "client.dns.lookup" -> "resolve_canonical_bootstrap_servers_only",
      "bootstrap.servers" -> "127.0.0.1:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "test-consumer-group",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    val kafkaTopics = List("myTopic")

    val kafkaRawStream: DStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream[String, String](
      sc, LocationStrategies.PreferConsistent, ConsumerStrategies.Subscribe[String, String](kafkaTopics, kafkaParams)
    )

    val weatherStream = kafkaRawStream.mapPartitions { partition =>
      partition.map(record => WeatherParser.weatherParsed(record.value()))
    }

    val weatherStream1Hour = weatherStream.window(Minutes(60))

    val weatherTemps1H = weatherStream1Hour.mapPartitions { partition =>
      partition.map(record => (record.dateTime, record.mainTemperature))
    }

    weatherTemps1H.foreachRDD { currentRdd =>
      val sparkSession = SparkSession.builder().config(currentRdd.sparkContext.getConf).getOrCreate()
      import sparkSession.implicits._

      val simpleDF: DataFrame = currentRdd.toDF()
      simpleDF.createOrReplaceTempView("simpleView")

      val countDF = sparkSession.sql("select count() as total from simpleView")
      countDF.show()

      val showAll = sparkSession.sql("select * from simpleView")
      showAll.show()

      val distinct = sparkSession.sql("select distinct * from simpleView")
      distinct.show()
    }

    sc.start()

    sc.awaitTermination()
  }
}
