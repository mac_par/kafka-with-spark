package com.connector.streaming

import com.connector.schema.WeatherParser
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Minutes, StreamingContext}

object WeatherStreaming {
  def main(args: Array[String]): Unit = {
    val cfg = new SparkConf().setMaster("local[*]").setAppName("WeatherWorker")
    val sc = new StreamingContext(cfg, Minutes(1))
    sc.sparkContext.setLogLevel("ERROR")

    val kafkaParams = Map[String, Object](
      "client.dns.lookup" -> "resolve_canonical_bootstrap_servers_only",
      "bootstrap.servers" -> "127.0.0.1:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "test-consumer-group",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    val kafkaTopics = List("myTopic")

    val kafkaRawStream: DStream[ConsumerRecord[String, String]] = KafkaUtils.createDirectStream[String, String](
      sc, LocationStrategies.PreferConsistent, ConsumerStrategies.Subscribe[String, String](kafkaTopics, kafkaParams)
    )

    val weatherStream = kafkaRawStream.mapPartitions{partition =>
      partition.map(record => WeatherParser.weatherParsed(record.value()))
    }

    val weatherStream1Hour = weatherStream.window(Minutes(60))

    val weatherTemps1H = weatherStream1Hour.mapPartitions{partition =>
      partition.map(record => (record.dateTime, record.mainTemperature))
    }

    weatherTemps1H.foreachRDD{currentRdd =>
      println(s"RDD content:\n\t${currentRdd.collect().map{case (time,temp) => s"Weather temperature was $temp at $time"}.mkString("\n\t")}")

      val temps: RDD[Double] = currentRdd.map(_._2)
      val minTemp: Option[Double] = if (temps.isEmpty()) None else Some(temps.min)
      val maxTemp: Option[Double] = if (temps.isEmpty()) None else Some(temps.max)

      println(s"Min temperature was ${if(minTemp.isDefined) minTemp.get.toString else "not defined"}")
      println(s"Max temperature was ${if(maxTemp.isDefined) maxTemp.get.toString else "not defined"}")
      println(s"Temperature difference: ${if(minTemp.isDefined && maxTemp.isDefined) (maxTemp.get - minTemp.get).toString}\n")
    }

    sc.start()

    sc.awaitTermination()
  }
}
