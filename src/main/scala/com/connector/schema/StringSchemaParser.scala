package com.connector.schema

import com.connector.source.KafkaSchemaParser
import org.apache.kafka.connect.data.Schema

class StringSchemaParser extends KafkaSchemaParser[String,String] {
  override val schema: Schema = Schema.STRING_SCHEMA

  override def output(input: String): String = input
}
