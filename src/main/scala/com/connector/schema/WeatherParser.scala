package com.connector.schema

import io.circe.parser.decode
import io.circe.Decoder
import org.joda.time.DateTime

object WeatherParser  extends Serializable {

  val emptyNumber = -1 // should have used None here
  val emptyString = "" // should have used None here

  case class WeatherSchema (
                             coordinateLongitude: Double,
                             coordinateLattitude: Double,

                             weatherId: Double,
                             weatherMain: String,
                             weatherDescription: String,
                             weatherIcon:String,

                             base: String,

                             mainTemperature: Double,
                             mainPressure: Double,
                             mainHumidity: Double,
                             mainTemperatureMin: Double,
                             mainTemperatureMax: Double,

                             visibility: Double,

                             windSpeed: Double,
                             windDegrees: Double,

                             cloudsAll:Double,

                             dt: Double,

                             sysType: Double,
                             sysId: Double,
                             sysMessage: Double,
                             sysCountry: String,
                             sysSunrise: Double,
                             sysSunset: Double,

                             id: Double,
                             name: String,
                             cod: Double
                           ) extends Serializable {
    def dateTime: String = dateTimeFromUnix(dt)
  }

  def dateTimeFromUnix(dt: Double): String = new DateTime(dt.toLong * 1000).toDateTime.toString("dd/MM/yyyy HH:mm:ss")

  object WeatherSchema {
    implicit val decoder: Decoder[WeatherSchema] = Decoder.instance { h =>
      for {
        coord_lon <- h.getOrElse[Double]("coord-lon")(emptyNumber)
        coord_lat <- h.getOrElse[Double]("coord-lat")(emptyNumber)

        weather_id <- h.getOrElse[Double]("weather-id")(emptyNumber)
        weather_main <- h.getOrElse[String]("weather-main")(emptyString)
        weather_description <- h.getOrElse[String]("weather-description")(emptyString)
        weather_icon <- h.getOrElse[String]("weather-icon")(emptyString)

        base <- h.getOrElse[String]("base")(emptyString)

        main_temp <- h.getOrElse[Double]("main-temp")(emptyNumber)
        main_pressure <- h.getOrElse[Double]("main-pressure")(emptyNumber)
        main_humidity <- h.getOrElse[Double]("main-humidity")(emptyNumber)
        main_temp_min <- h.getOrElse[Double]("main-temp-min")(emptyNumber)
        main_temp_max <- h.getOrElse[Double]("main-temp-max")(emptyNumber)

        visibility <- h.getOrElse[Double]("visibility")(emptyNumber)

        wind_speed <- h.getOrElse[Double]("wind-speed")(emptyNumber)
        wind_deg <- h.getOrElse[Double]("wind-deg")(emptyNumber)

        clouds_all <- h.getOrElse[Double]("clouds-all")(emptyNumber)

        dt <- h.getOrElse[Double]("dt")(emptyNumber)

        sys_type <- h.getOrElse[Double]("sys-type")(emptyNumber)
        sys_id <- h.getOrElse[Double]("sys-id")(emptyNumber)
        sys_message <- h.getOrElse[Double]("sys-message")(emptyNumber)
        sys_country <- h.getOrElse[String]("sys-country")(emptyString)
        sys_sunrise <- h.getOrElse[Double]("sys-sunrise")(emptyNumber)
        sys_sunset <- h.getOrElse[Double]("sys-sunset")(emptyNumber)

        id <- h.getOrElse[Double]("id")(emptyNumber)
        name <- h.getOrElse[String]("name")(emptyString)
        cod <- h.getOrElse[Double]("cod")(emptyNumber)

      } yield WeatherSchema(
        coord_lon,
        coord_lat,

        weather_id,
        weather_main,
        weather_description,
        weather_icon,

        base,

        main_temp,
        main_pressure,
        main_humidity,
        main_temp_min,
        main_temp_max,

        visibility,

        wind_speed,
        wind_deg,

        clouds_all,

        dt,

        sys_type,
        sys_id,
        sys_message,
        sys_country,
        sys_sunrise,
        sys_sunset,

        id,
        name,
        cod
      )
    }
  }

  val emptyWeatherSchema = WeatherSchema(
    emptyNumber,
    emptyNumber,

    emptyNumber,
    emptyString,
    emptyString,
    emptyString,

    emptyString,

    emptyNumber,
    emptyNumber,
    emptyNumber,
    emptyNumber,
    emptyNumber,

    emptyNumber,

    emptyNumber,
    emptyNumber,

    emptyNumber,

    emptyNumber,

    emptyNumber,
    emptyNumber,
    emptyNumber,
    emptyString,
    emptyNumber,
    emptyNumber,

    emptyNumber,
    emptyString,
    emptyNumber
  )


  def weatherParsed(structInput: String): WeatherSchema = {
    decode[WeatherSchema](structInput) match {
      case Left(error) => {
        //logger.error(s"JSON parser error: ${error}")
        emptyWeatherSchema
      }
      case Right(weather) => weather
    }
  }

  val weatherParser: String => WeatherSchema = (structInput: String) =>
    decode[WeatherSchema](structInput) match {
      case Left(error) => {
        //logger.error(s"JSON parser error: ${error}")
        emptyWeatherSchema
      }
      case Right(weather) => weather
    }
}
