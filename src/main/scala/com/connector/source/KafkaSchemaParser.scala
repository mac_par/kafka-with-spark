package com.connector.source

import org.apache.kafka.connect.data.Schema

trait KafkaSchemaParser[InputType, OutputType] {
  val schema: Schema
  def output(input: InputType): OutputType
}
